﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SH : MonoBehaviour//, Data_Observer
{

	#region PRIVATE_METHODS

	void Start ()
	{
		_Create_Data_Base ();
		//Data_Synchronizer.Instance.Register (this);
		Load_Scene_Data ();
	}

	void Load_Scene_Data ()
	{
		Load_Appropiate_Scene ();
		Load_Content_Data ();
	}

	void Load_Appropiate_Scene ()
	{
		//string current_Scene = SceneManager.GetActiveScene ().name;
	}


	void Load_Content_Data ()
	{
	}

	void _Create_Data_Base ()
	{
		DB_Access.Get_DB_Path (App_Config.DB_AT);
		DAO.Update_Table_Structure<DO_Client> ();
		DAO.Update_Table_Structure<DO_Instalations> ();
		DAO.Update_Table_Structure<DO_Legislation> ();
		DAO.Update_Table_Structure<DO_Offer> ();
		DAO.Update_Table_Structure<DO_Relation_Instalation_Legislation> ();
		DAO.Update_Table_Structure<DO_Relation_Legislation_Tasks> ();
		DAO.Update_Table_Structure<DO_Relation_Task_Task_Check> ();
		DAO.Update_Table_Structure<DO_Task> ();
		DAO.Update_Table_Structure<DO_Task_Check> ();
		DAO.Update_Table_Structure<DO_Technician> ();
	}

	#endregion

	#region PUBLIC_METHODS


	public void Update_Content_From_GH (DO Content)
	{/*
		Cache.Update_Content (Content);
		string table = Content.GetType ().Name;
		string json = JsonUtility.ToJson (Content);
		json= json.Replace ("{","");
		json = "{\"table\":\"" + table + "\"," + json;
		Data_Synchronizer.Instance.Send (json);
		*/
	}
		
	public void Disconnect(){
		//Data_Synchronizer.Instance.Disconnect ();
		Application.Quit();
	}
	#endregion
	#region Data_Observer implementation

	public void Data_Update (params DO[] data)
	{
		/*
		foreach (var item in data) {
			Cache.Update_Content (item);
		}
		string current_Scene = SceneManager.GetActiveScene ().name;

		if(data[0] is DO_Work_Order) {
			Debug.Log ("DO_Work_Order data recived");
		}else if(data[0] is DO_Rerun) {
			Debug.Log ("DO_Rerun data recived");
		}else if(data[0] is DO_Work_Block) {
			Debug.Log ("DO_Work_Block data recived");
		}else if(data[0] is DO_Work) {
			if (current_Scene.Equals (App_Config.Work)) {
				GH_Work gh_Work = GameObject.FindObjectOfType<GH_Work> ();
				foreach (var item in data) {
					gh_Work.Update_Content (item as DO_Work);
				}
			}
			Debug.Log ("DO_Work data recived");
		}else if(data[0] is DO_Work_Type) {
			Debug.Log ("DO_Work_Type data recived");
		}else if(data[0] is DO_Analysis) {
			if (current_Scene.Equals (App_Config.Analysis)) {
				GH_Analysis gh_Analysis = GameObject.FindObjectOfType<GH_Analysis> ();
				foreach (var item in data) {
					gh_Analysis.Update_Content (item as DO_Analysis);
				}
			}
			Debug.Log ("DO_Analysis data recived");
		}else if(data[0] is DO_Protected_Zone) {
			Debug.Log ("DO_Protected_Zone data recived");
		}else if(data[0] is DO_Protected_Zone_Check) {
			Debug.Log ("DO_Protected_Zone_Check data recived");
		}else if(data[0] is DO_Protected_Zone_Signature) {
			Debug.Log ("DO_Proctected_Zone_Signature data recived");
		}
		*/
	}

	public List<string> Get_Data_Types ()
	{
		return new List<string> (new string[] {
			/*
			"DO_Analysis",
			"DO_Proctected_Zone_Signature",
			"DO_Protected_Zone",
			"DO_Protected_Zone_Check",
			"DO_Rerun",
			"DO_Work",
			"DO_Work_Type",
			"DO_Work_Block",
			"DO_Work_Order",
			"DO_Work_Type"*/});
	}

	public List<int> Get_Data_Ids (string data_Types)
	{
		return new List<int> (new int[] { -1 });
	}

	#endregion

}
