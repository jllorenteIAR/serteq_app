﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SH_Navigator {

	#region PUBLIC_METHODS
	public static void Load_Add_Offer(){
		SceneManager.LoadScene (App_Config.Offer);
	}
	public static void Load_Add_Instalation(){
		SceneManager.LoadScene (App_Config.Instalation);
	}
	public static void Load_Add_Legislation(){
		SceneManager.LoadScene (App_Config.Legislation);
	}
	public static void Load_Task(){
		SceneManager.LoadScene(App_Config.Task);
	}
	public static void Load_Task_Check(){
		SceneManager.LoadScene (App_Config.Task_Check);
	}
	#endregion

}