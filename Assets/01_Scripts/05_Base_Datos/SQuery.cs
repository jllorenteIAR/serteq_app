﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SQuery : MonoBehaviour {

	#region SINGLETON
	static SQuery dao = Instance ();
	public static SQuery  Instance ()
	{
		if (dao == null) {
			dao = FindObjectOfType<SQuery> ();
			if (dao == null) {
				GameObject daoGO = GameObject.Find ("Segmented_Query");
				if (daoGO == null) {
					daoGO = new GameObject ();
					daoGO.name = "Segmented_Query";
				}
				dao = daoGO.AddComponent<SQuery> ();
			}
		}
		return dao;
	}
	#endregion
}
