﻿using SQLite4Unity3d;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DB_Access 
{
	private static Dictionary<string, DB_Access> _Dic_Instances = new Dictionary<string, DB_Access>();
	public SQLiteConnection connection;

	public static DB_Access Instance(string db) {
		if (!_Dic_Instances.ContainsKey(db)) {
			_Dic_Instances.Add (db, new DB_Access (db));
		}
		return _Dic_Instances[db];
	}

	static string DB_Path = "";
	private static string[] SDPath_Candidates = {"/storage/3937-3433/Android/data/com.iar.acciona.wtm.android/","/storage/extSdCard/com.iar.acciona.wtm.android/",Application.persistentDataPath+Path.DirectorySeparatorChar};

	public static string Get_DB_Path(string DatabaseName){
		
		#if UNITY_EDITOR //En el editor la BD esta en el proyecto en StreamingAssests
		DB_Path = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
		#elif UNITY_STANDALONE_WIN //En Windows Usamos la BD que esta en la carpeta DATA
		DB_Path = Application.dataPath + Path.DirectorySeparatorChar+"StreamingAssets"+Path.DirectorySeparatorChar + DatabaseName;
//			#endif
		#elif UNITY_ANDROID //En Android 
		foreach(string path in SDPath_Candidates){
			if (Directory.Exists(path)) {
				DB_Path = path + DatabaseName;
				Debug.Log("Path de android escogido: "+DB_Path);
				break;
			}
		}
		if (!File.Exists(DB_Path)) {
			Debug.Log("La BD no exite la copiamos desde assets");
			WWW DB_Origen = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
			while (!DB_Origen.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			// then save to Application.persistentDataPath
			File.WriteAllBytes(DB_Path, DB_Origen.bytes);
			Debug.Log("Copiada");
		}
		#endif

		return DB_Path;
	}

	private DB_Access (string db)
	{
		string dbPath = DB_Access.Get_DB_Path(db);
		connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
	}

	public static void Override_Table<T>(List<T> new_Content,string db){
		Instance(db).connection.DropTable<T> ();
		Instance(db).connection.CreateTable<T> ();
		Instance(db).connection.InsertAll (new_Content);
	}

	public static void Create_Table_If_Not_Exists<T>(string db){
		Instance(db).connection.CreateTable<T> ();
	}

	public static string Get_Table_Name<T>(){
		return typeof(T).Name;
	}


}
