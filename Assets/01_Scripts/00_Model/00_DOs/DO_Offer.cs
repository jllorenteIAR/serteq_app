﻿using SQLite4Unity3d;

public class DO_Offer : DO {
	#region PUBLIC_MEMBERS
	[PrimaryKey, AutoIncrement]
	public int Offer_ID {
		get{ return _Offer_ID; }
		set{ _Offer_ID = value; }
	}
	// As FK of DO_Instalation
	public int Instalation_ID {
		get{ return _Instalation_ID; }
		set{ _Instalation_ID = value; }
	}
	public float Budget {
		get{ return _Budget; }
		set{ _Budget = value; }
	}
	public bool Is_Accepted {
		get{ return _Is_Accepted; }
		set{ _Is_Accepted = value; }
	}
	#endregion

	#region PRIVATE_MEMBERS
	private int _Offer_ID;
	private int _Instalation_ID;
	private float _Budget;
	private bool _Is_Accepted;
	#endregion

}
