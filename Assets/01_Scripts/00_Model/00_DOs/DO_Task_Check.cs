﻿using SQLite4Unity3d;

public class DO_Task_Check : DO {

	#region PUBLIC_MEMBERS
	[PrimaryKey, AutoIncrement]
	public int Task_Check_ID {
		get{ return _Task_Check_ID; }
		set{ _Task_Check_ID = value; }
	}
	public string Response {
		get{ return _Response; }
		set{ _Response = value; }
	}
	#endregion
	#region PRIVATE_MEMBERS
	private int _Task_Check_ID;
	private string _Response;
	#endregion
}
