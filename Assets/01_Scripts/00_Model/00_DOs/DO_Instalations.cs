﻿using SQLite4Unity3d;

public class DO_Instalations : DO {
	#region PUBLIC_MEMBERS
	[PrimaryKey, AutoIncrement]
	public int Instalation_ID {
		get{ return _Instalation_ID; }
		set{ _Instalation_ID = value; }
	}
	// As FK of DO_Client
	public int Client_ID {
		get{ return _Client_ID; }
		set{ _Client_ID = value; }
	}
	public string Name {
		get{ return _Name; }
		set{ _Name = value; }
	}
	#endregion

	#region PRIVATE_MEMBERS
	private int _Instalation_ID;
	private int _Client_ID;
	private string _Name;
	#endregion
}
