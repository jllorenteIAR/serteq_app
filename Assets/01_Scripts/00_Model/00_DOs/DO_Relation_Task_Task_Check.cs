﻿using SQLite4Unity3d;

public class DO_Relation_Task_Task_Check : DO {

	#region PUBLIC_MEMBERS
	// As PK and FK of DO_Task
	public int Task_ID {
		get{ return _Task_ID; }
		set{ _Task_ID = value; }
	}
	// As PK and FK of DO_Task_Check
	public int Task_Checkc_ID {
		get{ return _Task_Checkc_ID; }
		set{ _Task_Checkc_ID = value; }
	}
	#endregion
	#region PRIVATE_MEMBERS
	private int _Task_ID;
	private int _Task_Checkc_ID;
	#endregion
}
