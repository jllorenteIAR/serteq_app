﻿using SQLite4Unity3d;

public class DO_Relation_Legislation_Tasks : DO {

	#region PUBLIC_MEMBERS
	// As PK and FK of DO_Legislation
	public int Legislation_ID {
		get{ return _Legislation_ID; }
		set{ _Legislation_ID = value; }
	}
	// As PK and FK of DO_Task
	public int Task_ID {
		get{ return _Task_ID; }
		set{ _Task_ID = value; }
	}
	#endregion
	#region PRIVATE_MEMBERS
	private int _Legislation_ID;
	private int _Task_ID;
	#endregion
}
