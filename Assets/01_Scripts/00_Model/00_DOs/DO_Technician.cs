﻿using SQLite4Unity3d;

public class DO_Technician : DO {

	#region PUBLIC_MEMBERS
	[PrimaryKey, AutoIncrement]
	public int Technician_ID {
		get{ return _Technician_ID; }
		set{ _Technician_ID = value; }
	}
	public string Name {
		get{ return _Name; }
		set{ _Name = value; }
	}
	#endregion
	#region PRIVATE_MEMBERS
	private int _Technician_ID;
	private string _Name;
	#endregion
}
