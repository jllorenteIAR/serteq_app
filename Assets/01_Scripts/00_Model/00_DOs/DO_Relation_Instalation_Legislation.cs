﻿using SQLite4Unity3d;

public class DO_Relation_Instalation_Legislation : DO {
	#region PUBLIC_MEMBERS
	// As PK and FK of DO_Instalation
	public int Instalation_ID {
		get{ return _Instalation_ID; }
		set{ _Instalation_ID = value; }
	}
	// As PK and FK of DO_Legislation
	public int Legislation_ID {
		get{ return _Legislation_ID; }
		set{ _Legislation_ID = value; }
	}
	#endregion
	#region PRIVATE_MEMBERS
	private int _Instalation_ID;
	private int _Legislation_ID;
	#endregion
}
