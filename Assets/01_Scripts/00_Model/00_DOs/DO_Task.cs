﻿using SQLite4Unity3d;
using System;

public class DO_Task : DO {

	#region PUBLIC_MEMBERS
	[PrimaryKey, AutoIncrement]
	public int Task_ID {
		get{ return _Task_ID; }
		set{ _Task_ID = value; }
	}
	// As FK of DO_Technician
	public int Technician_ID {
		get{ return _Technician_ID; }
		set{ _Technician_ID = value; }
	}
	public string Periodicity {
		get{ return _Periodicity; }
		set{ _Periodicity = value; }
	}
	public DateTime Last_Execute {
		get{ return _Last_Execute; }
		set{ _Last_Execute = value; }
	}
	#endregion
	#region PRIVATE_MEMBERS
	private int _Task_ID;
	private int _Technician_ID;
	private string _Periodicity;
	private DateTime _Last_Execute;
	#endregion
}
