﻿using  System.Collections.Generic;
using System;
using System.Reflection;
using System.Diagnostics;
using System.Collections;

//[System.Serializable]
public class DO: ICloneable
{
	
	public DO(){

	}

	#region ICloneable implementation
	/// <summary>
	/// Shallow copy of the element, reference variables won't be duplicated, will keep the same reference
	/// e.g. if the object cloned has a list, both objects will use the SAME list
	/// </summary>
	public object Clone ()
	{
		return (DO)this.MemberwiseClone ();
	}

	/// <summary>
	/// Deep clone of the element. Every parameter will be cloned. Unable to clone 2 layers of deepness in reference types
	/// e.g. List<List<int>>
	/// </summary>
	/// <returns>The clone.</returns>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public virtual T Deep_Clone<T>() where T:DO , new(){

		T orig = this as T;
		T clone = new T ();

		foreach (PropertyInfo property in clone.GetType ().GetProperties ()) {

			if (property.PropertyType.Namespace == "System.Collections.Generic") {
				if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition () == typeof(List<>)) {
					Type f = property.PropertyType.GetGenericArguments () [0];
					Type list_type = typeof(List<>).MakeGenericType (f);
					IList clone_list = (IList)Activator.CreateInstance (list_type);
					IList original_list = property.GetValue (orig, null) as IList;
					foreach (object value in original_list) {
						clone_list.Add (value);
					}
					property.SetValue (clone, clone_list, null);

				} else if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition () == typeof(Dictionary<,>)) {
					Type keyType = property.PropertyType.GetGenericArguments()[0];
					Type valueType = property.PropertyType.GetGenericArguments()[1];
					Type dict_type = typeof(Dictionary<,>).MakeGenericType (keyType, valueType);
					IDictionary clone_dict = (IDictionary)Activator.CreateInstance (dict_type);
					IDictionary original_dict = property.GetValue (orig, null) as IDictionary;

					foreach (var value in original_dict.Keys) {
						clone_dict.Add (value,original_dict[value]);
					}
					property.SetValue (clone, clone_dict, null);
				} 
			} else {
				var value = property.GetValue (orig, null);
				property.SetValue (clone, value, null);
			}
		}

		return clone;
	}

	#endregion

}

