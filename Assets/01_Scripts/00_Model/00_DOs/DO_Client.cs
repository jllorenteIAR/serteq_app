﻿using SQLite4Unity3d;

public class DO_Client : DO{
	#region PUBLIC_MEMBERS
	[PrimaryKey, AutoIncrement]
	public int Client_ID {
		get{ return _Client_ID; }
		set{ _Client_ID = value; }
	}
	public string CIF {
		get{ return _CIF; }
		set{ _CIF = value; }
	}
	public string Name {
		get{ return _Name; }
		set{ _Name = value; }
	}
	#endregion

	#region PRIVATE_MEMBERS
	private int _Client_ID;
	private string _CIF;
	private string _Name;
	#endregion
}
