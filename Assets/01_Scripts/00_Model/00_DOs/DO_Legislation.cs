﻿using SQLite4Unity3d;

public class DO_Legislation : DO {
	#region PUBLIC_MEMBERS
	[PrimaryKey, AutoIncrement]
	public int Legislation_ID {
		get{ return _Legislation_ID; }
		set{ _Legislation_ID = value; }
	}
	public string Name {
		get{ return _Name; }
		set{ _Name = value; }
	}
	#endregion

	#region PRIVATE_MEMBERS
	private int _Legislation_ID;
	private string _Name;
	#endregion
}
