﻿using System.Collections.Generic;

public abstract class DAO : Singleton<DAO> {

	public delegate void DAO_Response_DOs(List<DO> response, object Parametro = null);
	public delegate void DAO_Response_ERROR(string error, object Parametro = null);

	#region PUBLIC_METHODS
	public static void Create_Table_If_Not_Exists<T>(string dataBase = null) where T: DO {
		DB_Access.Create_Table_If_Not_Exists<T> (dataBase ?? App_Config.DB_AT);
	}
	public static List<T> Get_All<T>(string dataBase = null) where T: DO, new() {
		string query = "Select * from " + typeof(T).Name;
		List<T> lst_All = DB_Access.Instance(dataBase ?? App_Config.DB_AT).connection.Query<T> (query);
		return lst_All;
	}
	public static void Update_Table_Structure<T>(string dataBase = null) where T: DO, new() {
		Create_Table_If_Not_Exists<T> ();
		DB_Access.Override_Table<T> (Get_All<T> (), dataBase ?? App_Config.DB_AT);
	}
	#endregion
}