using UnityEngine;
[System.Serializable]
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T _Instance;

	public static T Instance {
		get {
			if (_Instance == null) {
				_Instance = MonoBehaviour.FindObjectOfType<T> ();
				if (_Instance == null) {
					GameObject Singleton = GameObject.Find ("Singleton");
					if (Singleton == null) {
						Singleton = new GameObject ("Singleton");
					}
					_Instance = Singleton.AddComponent<T> ();
				}
			}
			return _Instance;
		}
	}
}