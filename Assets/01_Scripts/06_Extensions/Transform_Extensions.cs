﻿using UnityEngine;
using System.Collections;

public static class Transform_Extensions
{

	public static void Set_Parent(this Transform transform, string parent_Name)
	{
		Transform parent = GameObject.Find (parent_Name).transform;
		transform.SetParent (parent);
		transform.localScale = new Vector3 (1,1,1);
	}
}