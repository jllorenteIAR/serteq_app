﻿using UnityEngine;
using System.Reflection;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class GH : MonoBehaviour {
	#region PUBLIC_METHODS
	public static GameObject Instantiate_Prefab(string prefab_Path, string parent){
		GameObject prefab = Instantiate (Resources.Load ("00_Prefabs/"+prefab_Path)) as GameObject;
		prefab.transform.Set_Parent (parent);
		return prefab;
	}

	public static GameObject Render_DO(string prefab_Path, string parent, DO d){
		// TODO Revisar esto.
		GameObject prefab = Instantiate (Resources.Load ("00_Prefabs/"+prefab_Path)) as GameObject;
				List<Text> txt_To_Change = prefab.GetComponents<Text> ().ToList();
				foreach(PropertyInfo property in d.GetType().GetProperties()){
					foreach (Text text in txt_To_Change.FindAll (t => t.name == property.Name)) {
						text.text = property.GetValue (property, null).ToString() ?? " ";
					}
				}
		prefab.transform.Set_Parent (parent);
		return prefab;
	}
	#endregion
}